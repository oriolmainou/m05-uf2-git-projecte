/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import view.View;
import controller.Controller;
import model.Model;

/**
 *
 * @authors Oriol Mainou i Guillem Parals
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Benvingut al joc 'Tres en linia'");
        Model model = new Model();
        View view = new View();
        Controller controller = new Controller(model, view);
    }

}
