/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Container;

/**
 *
 * @authors Oriol Mainou i Guillem Parals
 */
public class View extends JFrame {

    public JButton[] jbutton;

    public View() {
        this.setTitle("TRES EN LINIA");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addComponentsToPane(this.getContentPane());
        this.setResizable(true);
        this.pack();
        this.setVisible(true);
    }

    public void addComponentsToPane(Container pane) {
        JPanel jpanel = new JPanel(new GridLayout(3, 3));
        jbutton = new JButton[9];
        for (int i = 0; i < jbutton.length; i++) {
            jbutton[i] = new JButton(" ");
            jbutton[i].setPreferredSize(new Dimension(75, 75));
            jbutton[i].setName("" + (i + 1));
            jpanel.add(jbutton[i]);
        }
        pane.add(jpanel, BorderLayout.CENTER);
    }

    public void reset() {
        for (JButton button : jbutton) {
            button.setText("");
        }
    }

    public int showMessage(String message) {
        return JOptionPane.showOptionDialog(this, "Ha guanyat el jugador: " + message + "\nSeguim jugant?", "Tres en linia",
                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, new Object[]{"Si, seguim jugant", "Sortir del joc"}, "Seguir jugant");
    }
}
