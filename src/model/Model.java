/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @authors Oriol Mainou i Guillem Parals
 */
public class Model {

    protected int torn = 1, guanyador;
    protected String x = "X", o = "O";
    protected boolean error = false;
    protected String taulell[][] = {
        {"", "", ""},
        {"", "", ""},
        {"", "", ""}
    };

    public void reset() {
        for (int i = 0; i < taulell.length; i++) {
            for (int j = 0; j < taulell.length; j++) {
                taulell[i][j] = "";
            }
        }
        this.error = false;
        this.guanyador = 0;
        this.torn = 1;
    }

    public String setMoviment(int posicio) {
        String str = "";
        str = marcar(posicio, (torn == 1) ? this.x : this.o);
        if (checkGuanyador(this.x)) {
            this.guanyador = 1;
        } else if (checkGuanyador(this.o)) {
            this.guanyador = 2;
        } else if (fiJoc()) {
            this.guanyador = 3;
        }
        canviTorn();
        return str;
    }

    public void canviTorn() {
        torn = (torn == 1) ? 2 : 1;
        System.out.println("Es canvia el torn, " + getTorn());
    }

    public String marcar(int posicio, String value) {
        String marca = "";
        switch (posicio) {
            case 1:
                marca = sub_marcar(0, 0, value);
                break;
            case 2:
                marca = sub_marcar(0, 1, value);
                break;
            case 3:
                marca = sub_marcar(0, 2, value);
                break;
            case 4:
                marca = sub_marcar(1, 0, value);
                break;
            case 5:
                marca = sub_marcar(1, 1, value);
                break;
            case 6:
                marca = sub_marcar(1, 2, value);
                break;
            case 7:
                marca = sub_marcar(2, 0, value);
                break;
            case 8:
                marca = sub_marcar(2, 1, value);
                break;
            case 9:
                marca = sub_marcar(2, 2, value);
                break;
        }
        return marca;
    }

    public String sub_marcar(int x, int y, String value) {
        String marca = "";
        this.error = false;
        if (this.taulell[x][y].equals("")) {
            this.taulell[x][y] = value;
            marca = value;
        } else {
            marca = this.taulell[x][y];
            this.error = true;
        }
        return marca;
    }

    public boolean isError() {
        return error;
    }

    public int getGuanyador() {
        return guanyador;
    }

    public String getTorn() {
        return (this.torn == 1) ? "Torn: X" : "Torn: O";
    }

    public boolean checkGuanyador(String marca) {
        for (int i = 0; i < taulell.length; i++) {
            byte count = 0;
            for (int j = 0; j < taulell.length; j++) {
                count += (taulell[i][j].equals(marca)) ? 1 : 0;
            }
            if (count == 3) {
                return true;
            }
        }
        for (int j = 0; j < taulell.length; j++) {
            byte count = 0;
            for (int i = 0; i < taulell.length; i++) {
                count += (taulell[i][j].equals(marca)) ? 1 : 0;
            }
            if (count == 3) {
                return true;
            }
        }
        if (taulell[0][0].equals(marca) && taulell[1][1].equals(marca) && taulell[2][2].equals(marca)) {
            return true;
        }
        if (taulell[0][2].equals(marca) && taulell[1][1].equals(marca) && taulell[2][0].equals(marca)) {
            return true;
        }
        return false;
    }

    public boolean fiJoc() {
        for (int i = 0; i < taulell.length; i++) {
            for (int j = 0; j < taulell.length; j++) {
                if (taulell[i][j].equals("")) {
                    return false;
                }
            }
        }
        return true;
    }
}
