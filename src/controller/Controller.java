/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import model.Model;
import view.View;

/**
 *
 * @authors Oriol Mainou i Guillem Parals
 */
public class Controller implements ActionListener {

    protected Model model;
    protected View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
        for (JButton boto : view.jbutton) {
            boto.addActionListener(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        for (JButton jbutton : view.jbutton) {
            if (jbutton == event.getSource()) {
                int seleccio = -1;
                String s = model.setMoviment(Integer.parseInt(jbutton.getName()));
                jbutton.setText(s);

                if (model.getGuanyador() == 1) {
                    seleccio = view.showMessage(" 'X' ");
                } else if (model.getGuanyador() == 2) {
                    seleccio = view.showMessage(" 'O' ");
                } else if (model.getGuanyador() == 3) {
                    seleccio = view.showMessage(" Empat! ");
                }

                switch (seleccio) {
                    case 0: {
                        model.reset();
                        view.reset();
                        break;
                    }
                    case 1: {
                        System.exit(0);
                        break;
                    }
                }
            }
        }
    }
}
